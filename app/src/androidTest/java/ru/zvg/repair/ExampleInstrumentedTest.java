package ru.zvg.repair;

import android.content.Context;

import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.zvg.repair.dbsystem.db.ServiceDatabase;
import ru.zvg.repair.dbsystem.entities.Account;
import ru.zvg.repair.dbsystem.entities.Order;
import ru.zvg.repair.dbsystem.entities.Repairer;
import ru.zvg.repair.dbsystem.entities.Service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private Context context;
    private ServiceDatabase database;
    private Order order;
    private Account account;
    private Service service;
    private Repairer repairer;

    @Before
    public void prepareData() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        database = Room.inMemoryDatabaseBuilder(context, ServiceDatabase.class).build();
        prepareOrder();
        prepareRepairer();
        prepareAccount();
        prepareService();
    }

    private void prepareService() {
        service = new Service();
        service.setId(1);
        service.setType("type");
        service.setSubType("subtype");
        service.setPrice(1000);
    }

    private void prepareAccount() {
        account = new Account();
        account.setEmail("email");
        account.setPassword("password");
    }

    private void prepareRepairer() {
        repairer = new Repairer();
        repairer.setFirstName("firstName");
        repairer.setSecondName("secondName");
        repairer.setSphere("sphere");
        repairer.setId(1);
    }

    private void prepareOrder() {
        order = new Order();
        order.setServiceId(1);
        order.setRepairer("repairer");
        order.setOrderId(1);
        order.setModel("model");
        order.setBrand("brand");
        order.setEmail("email");
    }

    @Test
    public void testInsertAcc() { // тест был ли вставлен новый аккаунт в БД
        database.accountsDao().insert(account);
        Account temp = database.accountsDao().getAcc(account.getEmail());
        assertNotNull(temp);
    }

    @Test
    public void testInsertService() { /// тест была ли вставлена новая услуга в БД
        database.serviceDao().insert(service);
        Service temp = database.serviceDao().getService(service.getId());
        assertNotNull(temp);
    }

    @Test
    public void testInsertRepairer() { // тест был ли вставлен новый мастер в БД
        database.repairerDao().insert(repairer);
        Repairer temp = database.repairerDao().getRepairer(repairer.getId());
        assertNotNull(temp);
    }

    @Test
    public void testOrderInsert() { // тест был ли вставлен новый заказ в БД
        database.orderDao().insert(order);
        Order temp = database.orderDao().getOrder(order.getOrderId());
        assertNotNull(temp);
    }

    @Test
    public void testOrderDelete() { // тест на удаление заказа из БД
        database.orderDao().delete(order);
        assertNull(database.orderDao().getOrder(order.getOrderId()));
    }

    @Test
    public void testRepairerDelete() {// тест на удаление мастера из БД
        database.repairerDao().delete(repairer);
        assertNull(database.repairerDao().getRepairer(repairer.getId()));
    }

    @Test
    public void testAccDelete() { // тест на удаление аккаунта из бд
        database.accountsDao().delete(account);
        assertNull(database.accountsDao().getAcc(account.getEmail()));
    }

    @Test
    public void testServiceDelete() { // тест на удаление услуги из БД
        database.serviceDao().delete(service);
        assertNull(database.serviceDao().getService(service.getId()));
    }

    @After
    public void clearData() { //очищает данные
        context = null;
        repairer = null;
        order = null;
        account = null;
        service = null;
        database.close();
    }
}
