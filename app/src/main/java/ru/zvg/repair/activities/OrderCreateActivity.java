package ru.zvg.repair.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ru.zvg.repair.R;
import ru.zvg.repair.dbsystem.entities.Order;

public class OrderCreateActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinner;
    private EditText brandET;
    private EditText modelET;
    private Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_order_activity);
        spinner = findViewById(R.id.spinner);
        String[] names = getIntent().getStringArrayExtra("repairers");
        if (names != null) {
            RepairerAdapter adapter = new RepairerAdapter(this, android.R.layout.simple_list_item_1,
                    names);
            spinner.setAdapter(adapter);
        }
        brandET = findViewById(R.id.et_car_brand);
        modelET = findViewById(R.id.et_car_model);
        handler = new Handler();
        Button btn = findViewById(R.id.btn_add_order);
        btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        SharedPreferences preferences = getSharedPreferences("account", MODE_PRIVATE);
        final String email = preferences.getString("email", null);
        final long id = getIntent().getLongExtra("id", -1);
        Log.d("id", String.valueOf(id));
        if (email != null && id != -1) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Order order = new Order();
                    order.setEmail(email);
                    order.setServiceId(id);
                    order.setBrand(brandET.getText().toString());
                    order.setModel(modelET.getText().toString());
                    order.setRepairer(spinner.getSelectedItem().toString());
                    MainActivity.getInstance().getDatabase().orderDao().insert(order);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.getInstance(),
                                    "Заказ успешно оформлен", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
            }).start();
        } else {
            Toast.makeText(this, "Авторизуйтесь", Toast.LENGTH_SHORT).show();
        }


    }

    class RepairerAdapter extends ArrayAdapter<String> {

        RepairerAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
            super(context, resource, objects);
        }


    }


}
