package ru.zvg.repair.activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ru.zvg.repair.R;
import ru.zvg.repair.dbsystem.entities.Account;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);
        Button button = findViewById(R.id.btn_registration);
        button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        final EditText emailET = findViewById(R.id.et_email);
        final EditText passwordET = findViewById(R.id.et_password);
        if (emailET.getText().toString().trim().isEmpty()) {
            emailET.setError("Введите почту");
            return;
        }
        if (!emailET.getText().toString().trim().contains("@")) {
            emailET.setError("Неверный формат почты");
            return;
        }
        if (passwordET.getText().toString().trim().isEmpty()) {
            passwordET.setError("Введите пароль");
            return;
        }
        if (passwordET.getText().toString().trim().length() < 4) {
            passwordET.setError("Пароль короткий");
            return;
        }
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Account account = MainActivity.getInstance().
                        getDatabase().accountsDao().getAcc(emailET.getText().toString());
                if (account == null) {
                    account = new Account();
                    account.setPassword(passwordET.getText().toString().trim());
                    account.setEmail(emailET.getText().toString().trim());
                    MainActivity.getInstance().getDatabase().
                            accountsDao().insert(account);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.getInstance().getApplicationContext(),
                                    "Регистрация прошла успешно", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            emailET.setError("Почта занята");
                        }
                    });
                }
            }

        }).start();
    }
}
