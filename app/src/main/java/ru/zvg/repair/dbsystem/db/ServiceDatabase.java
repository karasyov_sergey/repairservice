package ru.zvg.repair.dbsystem.db;

import android.content.Context;
import android.content.res.AssetManager;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ru.zvg.repair.dbsystem.dao.AccountsDao;
import ru.zvg.repair.dbsystem.dao.OrderDao;
import ru.zvg.repair.dbsystem.dao.RepairerDao;
import ru.zvg.repair.dbsystem.dao.ServiceDao;
import ru.zvg.repair.dbsystem.entities.Account;
import ru.zvg.repair.dbsystem.entities.Order;
import ru.zvg.repair.dbsystem.entities.Repairer;
import ru.zvg.repair.dbsystem.entities.Service;

@SuppressWarnings("unused")
@Database(entities = {Account.class, Order.class, Repairer.class, Service.class},
        version = 1, exportSchema = false)
public abstract class ServiceDatabase extends RoomDatabase {

    public abstract AccountsDao accountsDao();

    public abstract OrderDao orderDao();

    public abstract RepairerDao repairerDao();

    public abstract ServiceDao serviceDao();

    private static ServiceDatabase database;

    public static ServiceDatabase getDB(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context, ServiceDatabase.class,
                    "service_base").build();
            database.init(context);
        }

        return database;
    }

    private void init(final Context context) {
        final AssetManager manager = context.getAssets();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (serviceDao().getAll().size() == 0) {
                    ArrayList<String> temp = new ArrayList<>();
                    try {
                        BufferedReader bf = new BufferedReader(new InputStreamReader(
                                manager.open("services.txt")));
                        String line;
                        while ((line = bf.readLine()) != null) {
                            temp.add(line);
                        }
                        bf.close();
                        for (int i = 0; i < temp.size(); i += 3) {
                            Service service = new Service();
                            service.setType(temp.get(i));
                            service.setSubType(temp.get(i + 1));
                            service.setPrice(Long.parseLong(temp.get(i + 2)));
                            serviceDao().insert(service);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setPriority(10);
        thread.start();
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                if (repairerDao().getAll().size() == 0) {
                    ArrayList<String> temp = new ArrayList<>();
                    try {
                        BufferedReader bf = new BufferedReader(
                                new InputStreamReader(manager.open("repairers.txt")));
                        String line;
                        while ((line = bf.readLine()) != null) {
                            temp.add(line);
                        }
                        bf.close();

                        for (int i = 0; i < temp.size(); i += 3) {
                            Repairer repairer = new Repairer();
                            repairer.setSecondName(temp.get(i));
                            repairer.setFirstName(temp.get(i + 1));
                            repairer.setSphere(temp.get(i + 2));
                            repairerDao().insert(repairer);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread1.setPriority(10);
        thread1.start();
        try {
            thread.join();
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
