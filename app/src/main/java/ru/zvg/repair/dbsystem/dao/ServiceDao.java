package ru.zvg.repair.dbsystem.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ru.zvg.repair.dbsystem.entities.Service;

@SuppressWarnings("unused")
@Dao
public interface ServiceDao {

    @Query("select * from services")
    List<Service> getAll();

    @Query("select sub_type from services where id = :id")
    String getSubType(long id);

    @Query("select price from services where id = :id")
    long getPrice(long id);

    @Query("select * from services where id = :id")
    Service getService(long id);

    @Insert
    void insert(Service service);

    @Update
    void update(Service service);

    @Delete
    void delete(Service service);

}
