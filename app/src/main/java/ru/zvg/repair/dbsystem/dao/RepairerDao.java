package ru.zvg.repair.dbsystem.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ru.zvg.repair.dbsystem.entities.Repairer;

@SuppressWarnings("unused")
@Dao
public interface RepairerDao {

    @Query("select * from repairers")
    List<Repairer> getAll();

    @Query("select * from repairers where service_sphere = :sphere")
    List<Repairer> getRepairers(String sphere);

    @Query("select * from repairers where id = :id")
    Repairer getRepairer(long id);


    @Insert
    void insert(Repairer r);

    @Update
    void update(Repairer r);

    @Delete
    void delete(Repairer r);
}
