package ru.zvg.repair.dbsystem.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import ru.zvg.repair.dbsystem.entities.Account;

@SuppressWarnings("unused")
@Dao
public interface AccountsDao {
    @Query("select * from accounts where email = :email")
    Account getAcc(String email);

    @Insert
    void insert(Account acc);

    @Update
    void update(Account acc);

    @Delete
    void delete(Account acc);
}
