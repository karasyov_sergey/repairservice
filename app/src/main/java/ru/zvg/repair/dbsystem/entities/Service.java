package ru.zvg.repair.dbsystem.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@SuppressWarnings("unused")
@Entity(tableName = "services")
public class Service {
    @PrimaryKey(autoGenerate = true)
    private long id;

    private String type;

    @ColumnInfo(name = "sub_type")
    private String subType;

    private long price;

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getSubType() {
        return subType;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
