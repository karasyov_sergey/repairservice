package ru.zvg.repair.dbsystem.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ru.zvg.repair.dbsystem.entities.Order;

@SuppressWarnings("unused")
@Dao
public interface OrderDao {

    @Query("select * from orders where email = :email")
    List<Order> getOrders(String email);

    @Query("select * from orders")
    List<Order> getAll();

    @Query("select * from orders where order_id = :id")
    Order getOrder(long id);

    @Insert
    void insert(Order o);

    @Update
    void update(Order o);

    @Delete
    void delete(Order o);
}
