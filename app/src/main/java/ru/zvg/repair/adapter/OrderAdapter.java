package ru.zvg.repair.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.zvg.repair.R;
import ru.zvg.repair.activities.MainActivity;
import ru.zvg.repair.dbsystem.db.ServiceDatabase;
import ru.zvg.repair.dbsystem.entities.Order;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderHolder> {

    private List<Order> orders;
    private Context context;
    private final Handler handler;


    public OrderAdapter(Context context, List<Order> orders) {
        this.context = context;
        this.orders = orders;
        handler = new Handler();
    }

    @NonNull
    @Override
    public OrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.order, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderHolder holder, int position) {
        final Order order = orders.get(position);
        holder.repairer.setText(String.format(
                context.getString(R.string.repairer), order.getRepairer()));
        holder.carModel.setText(String.format(
                context.getString(R.string.car_model), order.getModel()));
        holder.carBrand.setText(String.format(
                context.getString(R.string.car_brand), order.getBrand()));
        holder.id = position;
        final ServiceDatabase database = MainActivity.getInstance().getDatabase();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String subType = database.serviceDao().getSubType(order.getServiceId());
                final long price = database.serviceDao().getPrice(order.getServiceId());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        holder.price.setText(String.format(
                                context.getString(R.string.price), price));
                        holder.service.setText(String.format(
                                context.getString(R.string.service), subType));
                    }
                });
            }
        }).start();
    }


    @Override
    public int getItemCount() {
        return orders.size();
    }

    class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private int id;
        private TextView repairer;
        private TextView carBrand;
        private TextView carModel;
        private TextView service;
        private TextView price;


        OrderHolder(@NonNull View itemView) {
            super(itemView);
            repairer = itemView.findViewById(R.id.repairer);
            carBrand = itemView.findViewById(R.id.car_brand);
            carModel = itemView.findViewById(R.id.car_model);
            service = itemView.findViewById(R.id.service);
            price = itemView.findViewById(R.id.price);
            Button btn = itemView.findViewById(R.id.btn_cancel);
            btn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.getInstance().getDatabase().orderDao().delete(orders.get(id));
                    orders.remove(orders.get(id));
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    });
                }
            }).start();
        }
    }
}
