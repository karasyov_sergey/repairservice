package ru.zvg.repair.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.zvg.repair.R;
import ru.zvg.repair.activities.MainActivity;
import ru.zvg.repair.activities.OrderCreateActivity;
import ru.zvg.repair.dbsystem.entities.Repairer;
import ru.zvg.repair.dbsystem.entities.Service;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceHolder> {
    private List<Service> services;
    private Context context;
    private final Handler handler;

    public ServiceAdapter(Context context, List<Service> services) {
        this.context = context;
        this.services = services;
        handler = new Handler();
    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.service, parent, false);
        return new ServiceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHolder holder, int position) {
        Service service = services.get(position);
        setVisibleData(holder, service);
    }

    private void setVisibleData(ServiceHolder holder, Service service) {
        holder.type.setText(String.format(
                context.getString(R.string.service_type), service.getType()));
        holder.subType.setText(String.format(
                context.getString(R.string.work_type), service.getSubType()));
        holder.price.setText(String.format(
                context.getString(R.string.price), service.getPrice()));
        holder.id = service.getId();
    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    class ServiceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView type;
        private TextView subType;
        private TextView price;
        private long id;

        ServiceHolder(@NonNull View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.type);
            subType = itemView.findViewById(R.id.sub_type);
            price = itemView.findViewById(R.id.price);
            Button btn = itemView.findViewById(R.id.btn_create_order);
            btn.setOnClickListener(this);

        }

        @Override
        public void onClick(final View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<Repairer> repairers = MainActivity.getInstance().getDatabase().
                            repairerDao().getRepairers(services.get((int) (id - 1)).getType());

                    final Intent intent = new Intent(MainActivity.getInstance(),
                            OrderCreateActivity.class);
                    String[] names = new String[repairers.size()];
                    for (int i = 0; i < names.length; i++) {
                        names[i] = repairers.get(i).getFirstName() + " " + repairers.get(i).getSecondName();
                    }
                    intent.putExtra("repairers", names);
                    intent.putExtra("id", id);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            v.getContext().startActivity(intent);
                        }
                    });
                }
            }).start();
        }
    }
}
