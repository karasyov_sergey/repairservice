package ru.zvg.repair.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import ru.zvg.repair.activities.MainActivity;
import ru.zvg.repair.R;
import ru.zvg.repair.activities.RegistrationActivity;
import ru.zvg.repair.dbsystem.db.ServiceDatabase;
import ru.zvg.repair.dbsystem.entities.Account;

public class LogInFragment extends Fragment implements View.OnClickListener {
    private EditText emailET;
    private EditText passwordET;
    private MainActivity instance;
    private Handler handler;
    private ServiceDatabase database;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_log_in, container, false);
        initialize(root);
        return root;
    }

    private void initialize(View root) {
        emailET = root.findViewById(R.id.et_email);
        passwordET = root.findViewById(R.id.et_password);
        instance = MainActivity.getInstance();
        handler = new Handler();
        database = instance.getDatabase();
        Button btnAuthorization = root.findViewById(R.id.btn_authorization);
        Button btnRegistration = root.findViewById(R.id.btn_to_registration);
        btnAuthorization.setOnClickListener(this);
        btnRegistration.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_authorization:
                startAuthorization();
                break;
            case R.id.btn_to_registration:
                startRegistration();
                break;
        }
    }

    private void startAuthorization() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Account account = database.accountsDao().
                        getAcc(emailET.getText().toString());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (account != null) {
                            Toast.makeText(instance.getApplicationContext(),
                                    "Авторизация прошла успешно",
                                    Toast.LENGTH_SHORT).show();
                            SharedPreferences preferences = instance.getApplicationContext().
                                    getSharedPreferences("account", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("email", emailET.getText().toString());
                            editor.apply();
                            instance.setVisibleFragment(R.id.navigation_service);
                        } else {
                            emailET.setError("Неверные данные");
                            passwordET.setError("Неверные данные");
                        }
                    }
                });

            }
        }).start();
    }

    private void startRegistration() {
        startActivity(new Intent(instance, RegistrationActivity.class));
    }
}