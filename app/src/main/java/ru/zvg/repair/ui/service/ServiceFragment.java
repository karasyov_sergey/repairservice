package ru.zvg.repair.ui.service;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.zvg.repair.activities.MainActivity;
import ru.zvg.repair.R;
import ru.zvg.repair.adapter.ServiceAdapter;
import ru.zvg.repair.dbsystem.entities.Service;

public class ServiceFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_service, container, false);
        initialize(root);
        return root;
    }

    private void initialize(final View root) {
        final RecyclerView recyclerView = root.findViewById(R.id.rec_view);
        LinearLayoutManager manager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(manager);
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Service> services = MainActivity.getInstance().getDatabase().serviceDao().getAll();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        ServiceAdapter adapter = new ServiceAdapter(root.getContext(), services);
                        recyclerView.setAdapter(adapter);
                    }
                });
            }
        }).start();


    }
}