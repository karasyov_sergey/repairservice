package ru.zvg.repair.ui.orders;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.zvg.repair.R;
import ru.zvg.repair.activities.MainActivity;
import ru.zvg.repair.adapter.OrderAdapter;
import ru.zvg.repair.dbsystem.entities.Order;

public class OrdersFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_orders, container, false);
        initialize(root);

        return root;
    }

    private void initialize(final View root) {
        final RecyclerView recyclerView = root.findViewById(R.id.rec_view_orders);
        LinearLayoutManager manager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(manager);
        final Handler handler = new Handler();
        SharedPreferences preferences = root.getContext().
                getSharedPreferences("account", Context.MODE_PRIVATE);
        final String email = preferences.getString("email", null);

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (email != null) {
                    final List<Order> orders =
                            MainActivity.getInstance().getDatabase().
                                    orderDao().getOrders(email);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            OrderAdapter adapter = new OrderAdapter(root.getContext(), orders);
                            recyclerView.setAdapter(adapter);
                        }
                    });
                }
            }
        }).start();
    }
}